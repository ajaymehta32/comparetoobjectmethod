package ajaymehta.comparetoobjectmethod;

/**
 * Created by Avi Hacker on 7/15/2017.
 */

// IT behaves same as compareTo method of Comparable interface...blah blah blah .....

public class Case2 {


    public static Integer largest() {

        Integer x =3;
        Integer y =5;

        // TODO...comment bigger in else condition ..it will ask to initilize ...coz ..if else false so bigger will not execute ...but we need bigger to return..
        Integer bigger=0 ;
// it gives result in true and false
     if(x.compareTo(y) > 0) {  // > 0  if(x > y)          means it gives taste of boolean if(true) !  this condition is false ... x is not greater than 5 ..so else condition will exectue..

         bigger = x;
     } else {  // it will execute n print value of y (5)

         bigger = y;
     }

         return  bigger;
     }

    // lets make if condition false ...


    public static Integer largest2() {

        Integer x =3;
        Integer y =5;

        Integer bigger ;  // we dont need to initilize it ..coz its outside ..if  (in else ) so it will execute..

        if(x.compareTo(y) < 0) {  // < 0   it like  if( x <  y)  ..yes

            bigger = x;  // so this will work
        } else {

            bigger = y;
        }

        return  bigger;
    }









    public static void main(String args[]) {

       System.out.println(largest());
        System.out.println("=======================");
       System.out.println(largest2());

    }

}
