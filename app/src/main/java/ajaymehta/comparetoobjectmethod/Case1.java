package ajaymehta.comparetoobjectmethod;

/**
 * Created by Avi Hacker on 7/15/2017.
 */

// we can't apply  arithmetic operations or any other ( that we do on integers, double etc ).... on Objects ....
    // so here we are gonna use a method ( a operation ) to compare objects..
public class Case1 {

    // compareTo  (method for comparing object from Double, Integer class etc) ..is avail to wrapper classes (coz we know what type of object we have )

    // compareTo operation can be applied on  Byte, Double, Integer, Float, Long, or Short.
    public static void largest1() {

        Integer x =3;

        System.out.println(x.compareTo(5));  // x is smaller it will return -1
        System.out.println(x.compareTo(1));  // x is greater it will return 1
        System.out.println(x.compareTo(3));  // x is equal it will return 0
        System.out.println("=====================================");


// it will print bolean value  > 0  compare n give boolean response
        System.out.println(x.compareTo(5) > 0);  // x (3) greater than 5  ..NO ..false
        System.out.println(x.compareTo(1) > 0);  // x is greater than 1  yes ...true
        System.out.println(x.compareTo(3) == 0);  // x ..yes true..
        System.out.println("=====================================");

    }

    public static void largest2() {

        Double x =3.5;

        System.out.println(x.compareTo(5.4));  // x is smaller it will return -1
        System.out.println(x.compareTo(1.2));  // x is greater it will return 1
        System.out.println(x.compareTo(3.5));  // x is equal it will return 0
        System.out.println("=====================================");

    }


    public static void main(String args[]) {

        largest1();
        largest2();
    }
}
